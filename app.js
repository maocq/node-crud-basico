var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var multer = require('multer');
var cloudinary = require('cloudinary');
var method_override = require('method-override');
var Schema = mongoose.Schema;

cloudinary.config({
  cloud_name: "dcgxxxwpz",
  api_key: "619533373825399",
  api_secret: "uA-BGgFQVFwYHxQ4SNgtwAm5rEU"
});

var app = express();

mongoose.connect("mongodb://localhost/primera_pagina");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(multer({dest: "./uploads "}));
app.use(method_override("_method"));

// Shema productos
var productSchemaJSON = {
  title: String,
  description: String,
  imageUrl: String,
  pricing: Number
};

var productSchema = new Schema(productSchemaJSON);

productSchema.virtual("image.url").get(function(){
  if(this.imageUrl === "" || this.imageUrl === "data.png") {
    return "default.png";
  }
  return this.imageUrl;
});

var Product = mongoose.model("Product", productSchema);

app.set("view engine", "jade");

app.use(express.static("public"));

app.get("/", function(req, res){
  res.render("index");
});

app.get("/menu/new", function(req, res){
  res.render("menu/new");
});

app.get("/menu", function(req, res){
  // Product.findOne
  Product.find(function(error, documento){
    if (error) { console.log(error); }
    res.render("menu/index", { products: documento });
  });
});

app.get("/admin", function(req, res){
  Product.find(function(error, documento){
    if (error) { console.log(error); }
    res.render("admin/index", { products: documento });
  });
});

app.post("/menu", function(req, res){

  var data = {
    title: req.body.title,
    description: req.body.description,
    imageUrl: "data.png",
    pricing: req.body.pricing
  };

  var product = new Product(data);

  if (req.files.hasOwnProperty('image_avatar')) {
    cloudinary.uploader.upload(
      req.files.image_avatar.path,
      function(result) {
        product.imageUrl = result.url;
        product.save(function(error){
          console.log(product);
          res.redirect("/menu");
        });
      }
    );
  }else{
    product.save(function(error){
      console.log(product);
      res.redirect("/menu");
    });
  }

});

app.get("/menu/edit/:id", function(req, res){
  var id_producto = req.params.id
  Product.findOne({"_id": id_producto}, function(error, producto){
    console.log(producto);
    res.render("menu/edit", { product: producto });
  });
});

app.put("/menu/:id", function(req, res){

  var data = {
    title: req.body.title,
    description: req.body.description,
    pricing: req.body.pricing
  };

  if (req.files.hasOwnProperty('image_avatar')) {
    cloudinary.uploader.upload(
      req.files.image_avatar.path,
      function(result) {
        data.imageUrl = result.url;
        Product.update({"_id": req.params.id}, data, function(){
          res.redirect("/menu");
        });
      }
    );
  }else{
    Product.update({"_id": req.params.id}, data, function(){
      res.redirect("/menu");
    });
  }

});

app.delete("/menu/:id", function(req, res){
    Product.remove({"_id":req.params.id}, function(error){
      if (error) { console.log(error); }
      res.redirect("/admin");
    });
});

app.listen(8080);